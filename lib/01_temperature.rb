# functions to convert f to c

def ftoc(f)
  (f - 32) * 5 / 9.0
end

def ctof(c)
  c * 9.0 / 5 + 32
end

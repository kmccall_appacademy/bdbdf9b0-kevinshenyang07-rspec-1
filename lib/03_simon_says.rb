# udf

def echo(content)
  content
end

def shout(content)
  content.upcase
end

def repeat(content, times=2)
  output = [content] * times
  output.join(' ')
end

def start_of_word(content, pointer)
  content[0..pointer-1]
end

def first_word(content)
  content.split(' ')[0]
end

def titleize(content)
  output = []
  arr_content = content.split(' ')
  arr_content.each_index do |i|
    word = arr_content[i]
    if i == 0 or (not ['the', 'over', 'and'].include?(word))
      word = word.capitalize
    end
    output << word
  end
  output.join(' ')
end
